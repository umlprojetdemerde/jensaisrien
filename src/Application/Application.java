package Application;

import javax.swing.JFrame;

import Controleurs.Evenement;
import Vues.Formulaire;

public class Application extends JFrame{

	private static final long serialVersionUID = 1L;

	public Application() {
		//On d�fini le titre de la fenetre
		setTitle("Inscription Semi-Marathon");
		
		//On d�fini la taille de la fenetre et le fait qu'elle ne peut pas etre redimensionn�e
		setResizable(false);
		setSize(350, 375);
		
		//Fermeture de l'application en appuyant sur la petite croix 
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		
		/*
		* On a programm� selon MVC
		* On cr�er un objet de type application
		* Celui-ci contient le formulaire d'enregistrement
		* Enfin, on rajoute un objet "evenement", qui regroupe les listeners du formulaire
		*/
		
		Application appli = new Application();
		Formulaire formulaire = new Formulaire();
		Evenement eventTest = new Evenement(formulaire);
		formulaire.ajouterEcouteur(eventTest);
		appli.add(formulaire);
		appli.setVisible(true);

	}

}
