package Vues;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Controleurs.Evenement;

public class Formulaire extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//On d�clare un tableau de String qui servira pour le JComboBox afin de selectionner la civilite

	private String civilite[] = {"", "Monsieur", "Madame"}; 

	//On cr�e deux pannels sur lesquels nous allons rajouter les boutons ainsi que les labels/txtlabels
	
	JPanel panGlobal;
	JPanel panBoutons;

	//On d�clare les label, textfield, checkbox, combobox et bouttons en tant que attributs (pas dans un tableau
	//pour un soucis de flexibilit�
	
	JLabel titre;
	JLabel labCivilite;
	JLabel labNom;
	JTextField txtNom;
	JLabel labPrenom;
	JTextField txtPrenom;
	JLabel labAnneeNaiss;
	JTextField txtAnneeNaiss;
	JLabel labAdresse;
	JTextField txtAdresse;
	JLabel labVille;
	JTextField txtVille;
	JLabel labPays;
	JTextField txtPays;
	JLabel labNationalite;
	JTextField txtNationalite;
	JLabel labEmail;
	JTextField txtEmail;
	JLabel labTel;
	JTextField txtTel;
	JLabel labCertifMedical;
	
	JCheckBox chkCertifMedical;

	JButton btnInscrire;
	JButton btnQuitter;

	JComboBox civiliteList;
	
	//Le constructeur de la classe Formulaire

	public Formulaire() {

		titre = new JLabel("Veuillez saisir vos coordonn�es afin de vous inscire:");
		labCivilite = new JLabel("Civilit�:");
		civiliteList = new JComboBox(civilite);
		labNom = new JLabel("Nom:");
		txtNom = new JTextField();
		labPrenom = new JLabel("Prenom:");
		txtPrenom = new JTextField();
		labAnneeNaiss = new JLabel("Ann�e naissance:");
		txtAnneeNaiss = new JTextField();
		labAdresse = new JLabel("Adresse:");
		txtAdresse = new JTextField();
		labVille = new JLabel("Ville:");
		txtVille = new JTextField();
		labPays = new JLabel("Pays:");
		txtPays = new JTextField();
		labNationalite = new JLabel("Nationalit�:");
		txtNationalite = new JTextField();
		labEmail = new JLabel("email:");
		txtEmail = new JTextField();
		labTel = new JLabel("T�l�phone");
		txtTel = new JTextField();
		labCertifMedical = new JLabel("Certificat m�dical:");
		chkCertifMedical = new JCheckBox("Datant de moins d'un an");

		
		btnInscrire = new JButton("S'inscrire");
		btnQuitter = new JButton("Quitter");

		panGlobal = new JPanel();
		panGlobal.setLayout(new GridLayout(11, 2));
		panGlobal.add(labCivilite);
		panGlobal.add(civiliteList);
		panGlobal.add(labNom);
		panGlobal.add(txtNom);
		panGlobal.add(labPrenom);
		panGlobal.add(txtPrenom);
		panGlobal.add(labAnneeNaiss);
		panGlobal.add(txtAnneeNaiss);
		panGlobal.add(labAdresse);
		panGlobal.add(txtAdresse);
		panGlobal.add(labVille);
		panGlobal.add(txtVille);
		panGlobal.add(labPays);
		panGlobal.add(txtPays);
		panGlobal.add(labNationalite);
		panGlobal.add(txtNationalite);
		panGlobal.add(labEmail);
		panGlobal.add(txtEmail);
		panGlobal.add(labTel);
		panGlobal.add(txtTel);
		panGlobal.add(labCertifMedical);
		panGlobal.add(chkCertifMedical);

		panBoutons = new JPanel();
		panBoutons.setLayout(new GridLayout(1, 2));
		panBoutons.add(btnInscrire);
		panBoutons.add(btnQuitter);
		
		/*
		 * On rajoute a cet objet le titre au nord, les boutons au sud et le global
		 */
		add(BorderLayout.NORTH, titre);
		add(panGlobal);
		add(BorderLayout.SOUTH, panBoutons);

	}

	// Methode pour ajouter un ecouteur pass� en parametre aux deux boutons
	
	public void ajouterEcouteur(Evenement evenement)
	{
		btnInscrire.addActionListener(evenement);
		btnQuitter.addActionListener(evenement);
	}
	
	//methode pour reinitialiser les champs de text lorsqu'on inscrit une personne afin de pouvoir inscrire une autre plus facilement
	public void reinitialiser() {
		txtNom.setText("");
		txtPrenom.setText("");
		txtAdresse.setText("");
		txtEmail.setText("");
		txtAnneeNaiss.setText("");
		txtVille.setText("");
		txtPays.setText("");
		txtNationalite.setText("");
		txtTel.setText("");
		civiliteList.setSelectedIndex(0);
		chkCertifMedical.setSelected(false);
	}
	
	
	// Les diff�rents getters et setters dont on a besoin pour le formulaire
	
	public JComboBox getCiviliteList() {
		return civiliteList;
	}
	
	public String getTxtNom() {
		return txtNom.getText();
	}
	
	public String getTxtPrenom() {
		return txtPrenom.getText();
	}

	public String getTxtAnneeNaiss() {
		return txtAnneeNaiss.getText();
	}

	public String getTxtAdresse() {
		return txtAdresse.getText();
	}

	public String getTxtVille() {
		return txtVille.getText();
	}

	public String getTxtPays() {
		return txtPays.getText();
	}

	public String getTxtNationalite() {
		return txtNationalite.getText();
	}

	public String getTxtEmail() {
		return txtEmail.getText();
	}

	public String getTxtTel() {
		return txtTel.getText();
	}

	public JCheckBox getChkCertifMedical() {
		return chkCertifMedical;
	}
	
}
