package Modele;
/**
 * Cette classe permet de lier un coureur � son entrepirse ou � son Ecole (on ne distingue pas les 2)
 */
public class EcoleEntreprise {
	
	public final int NB_MAX_COUREURS = 40000;

	Coureur[] sesCoureurs;
	private String nom;
	private int nbCoureurs; //

	// CONSTRUCTEUR
	public EcoleEntreprise(String nom) {
		super();
		this.nom = nom;
	}
	
	// METHODES
	public void addCoureur(Coureur unCoureur){
		if (nbCoureurs < NB_MAX_COUREURS){
			sesCoureurs[nbCoureurs]=unCoureur;
			nbCoureurs++;
		}
	}


	// GETTEURS & SETTEURS
	public int getNbCoureurs() {
		return this.nbCoureurs = nbCoureurs; // retourne le nb de coureur de l'�cole/ entreprise
	}
	
	
}