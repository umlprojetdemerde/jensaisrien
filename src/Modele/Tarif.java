package Modele;
import java.sql.Date;
public class Tarif {

	private Date dateDebut;
	private Date dateFin;
	private double prix;

	// Constructeur
	public Tarif(Date dateDebut, Date dateFin, double prix) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.prix = prix;
	}

}