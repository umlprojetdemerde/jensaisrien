package Modele;
public class TypeDossard {
	Coureur[] sesCoureurs;
	private String libelle;
	private String description;

	// Constructeurs
	public TypeDossard(String libelle, String description) {
		super();
		this.libelle = libelle;
		this.description = description;
	}
}