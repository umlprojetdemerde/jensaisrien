package Modele;
public class PointControle {

	Rue saRue;
	ChronometragePoint[] sesHeuresChronometrage;
	private double km;
	
	// Constructeur
	public PointControle(Rue saRue, ChronometragePoint[] sesHeuresChronometrage, double km) {
		super();
		this.saRue = saRue;
		this.sesHeuresChronometrage = sesHeuresChronometrage;
		this.km = km;
	}
}