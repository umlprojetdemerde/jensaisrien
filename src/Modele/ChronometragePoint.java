package Modele;
import java.sql.Time;

public class ChronometragePoint {

	PointControle sonPointControle;
	Coureur sonCoureur;
	private Time heurePointControle;

	public ChronometragePoint(PointControle sonPointControle, Coureur sonCoureur) {
		this.sonPointControle = sonPointControle;
		this.sonCoureur = sonCoureur;
	}
}