package Modele;
public class Categorie {

	private String nom;
	private String code;
	private int anneeDeb;
	private int anneeFin;
	
	public Categorie(String nom, String code, int anneeDeb, int anneeFin) {
		super();
		this.nom = nom;
		this.code = code;
		this.anneeDeb = anneeDeb;
		this.anneeFin = anneeFin;
	}
}