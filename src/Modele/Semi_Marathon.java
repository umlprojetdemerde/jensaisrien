package Modele;

public class Semi_Marathon {
	final private int NBMAXPARTICIPANTS=40000; 

	Rue[] sesRues;
	int nbRues=0;
	Partenaire[] sesPartenaires;
	int nbPartenaires=0;
	Tarif[] sesTarifs;
	int nbTarifs=0;
	TypeDossard[] sesTypesDossards;
	int nbTypeDossards=0;
	EcoleEntreprise[] sesEcolesEntreprises;
	int nbEcoleEntreprises=0;
	Coureur[] sesInscrits = new Coureur[NBMAXPARTICIPANTS]; 
	Categorie[] sesCategories;
	int nbCategories=0;
	
	private String date="8 Mars 2015"; // 8 mars 2015


	private String ville="Paris";
	private String lieuDepartArret="Chateau de Vincennes";
	private String URL="http://www.semimarathondeparis.fr";
	private String adresseInscription="ASO Challenges gnagnagna 75000 PARIS";
	private String lieuRemiseDossard="Parc Floral";
	private String horairesRemiseDossard ="6 Mars de 10h � 18h - 7 Mars de 10h � 19h - 8 mars de 7h � 9h";
	

	private String heureDepart="10h";
	private String tempsMax="2h50";
	
	private double longueur =21.1; // Longueur du semi marathon
	
	private int anneeNaissanceMin=1997;

	private int nbInscrits=0;
	// Attribut calcul�
	private int nbPlacesDispo=NBMAXPARTICIPANTS;
	
	/*
	 * getNbPlacesDispo => Attribut calcul�, nbPlacesDispo = nbMaxParticipants - nbInscrits
	 */
	public int getNbPlacesDispo(){
		return (this.NBMAXPARTICIPANTS-this.nbInscrits);
	}
	
	// Methode poura rajouter un coureur a un marathon
	public void ajouterCoureur(Coureur coureurAjouter) {
		if (nbInscrits < NBMAXPARTICIPANTS){
		sesInscrits[nbInscrits]=coureurAjouter;
		nbInscrits++;
		}
	}

	// Methode pour recuperer tous les coureurs inscrits
	public Coureur[] getSesInscrits() {
		return sesInscrits;
	}
	
	// Ajouter un partenaire
	public void ajouterPartenaire(Partenaire partenaireAjouter) {
		sesPartenaires[nbPartenaires]=partenaireAjouter;
		nbPartenaires++;
		}
	
	// Ajouter une ecole entreprise
	public void ajouterEcoleEntreprise(EcoleEntreprise ecoleEntrepriseAjouter) {
		sesEcolesEntreprises[nbEcoleEntreprises]=ecoleEntrepriseAjouter;
		nbEcoleEntreprises++;
		}
	
	// Ajouter tarif
	public void ajouterTarif(Tarif tarifAjouter) {
		sesTarifs[nbTarifs]=tarifAjouter;
		nbTarifs++;
		}
	
	// Ajouter typeDossard
	public void ajouterTypeDossard(TypeDossard typeDossardAjouter) {
		sesTypesDossards[nbTypeDossards]=typeDossardAjouter;
		nbTypeDossards++;
		}
	
	// Ajouter une rue
	public void ajouterRue(Rue rueAjouter) {
		sesRues[nbRues]=rueAjouter;
		nbRues++;
		}
	
	// Ajouter une categorie
	public void ajouterCategorie(Categorie categorieAjouter) {
		sesCategories[nbTarifs]=categorieAjouter;
		nbCategories++;
		}
	
	// Retourne le nombre max de participans
	public int getNBMAXPARTICIPANTS() {
		return NBMAXPARTICIPANTS;
	}
}