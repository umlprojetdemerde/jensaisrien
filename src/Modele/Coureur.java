package Modele;
import java.sql.Date;
import java.sql.Time;

public class Coureur {

	ChronometragePoint[] sesHeuresChronometrage;
	EcoleEntreprise sonEcoleEntreprise;
	TypeDossard sonTypeDossard;

	private static int numCoureur; 
	
	private int numDossard;
	private String nom;
	private String prenom;
	private char sexe; // H pour homme ; F pour femme
	private int anneeNaissance;
	private String adresse;
	private int codePostal;
	private String ville;
	private String etat;
	private String pays;
	private String nationalite;
	private String tel;
	private String email;
	private String club;
	private int numLicence;
	private boolean licenceCopie; // Permet de v�rifier que la copie de licence a �t� envoy� avec le dossier
	private String typePaiement; // Soit cheque, soit CB
	private long numCB;
	private int cb_CSC; // Code de v�rification en 3 caract�res
	private int moisFinCB;
	private int anneeFinCB;
	private boolean paiementOK; // Permet de v�rifier que l'inscription a �t� pay�e
	private boolean certificatMedical; //  true si le certificat m�dical est pr�sent
	private Date dateInscription;
	private boolean dossardRetire; // Permet de v�rifier que le dossard � bien �t� retir�
	private Time tempsReel;
	private int position;
	
	// Constructeur avec 0 parametres qui attribue a chaque objet son numero de dossard
	public Coureur() {
		numCoureur++;
		numDossard=numCoureur;
	}
	
	// Setters pour les attributs du formulaire

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setAnneeNaissance(int anneeNaissance) {
		this.anneeNaissance = anneeNaissance;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setCertificatMedical(boolean certificatMedical) {
		this.certificatMedical = certificatMedical;
	}
	
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	// Permet d'afficher dans la console le r�sultat d'une inscription
	@Override
	public String toString() {
		return "Coureur [numDossard=" + numDossard + ", nom=" + nom + ", prenom=" + prenom + ", sexe=" + sexe
				+ ", anneeNaissance=" + anneeNaissance + ", adresse=" + adresse + ", ville=" + ville + ", pays=" + pays
				+ ", nationalite=" + nationalite + ", tel=" + tel + ", email=" + email + ", certificatMedical="
				+ certificatMedical + ", dateInscription=" + dateInscription + "]";
	}

	
	
	

}