package Controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

import javax.swing.JButton;

import Modele.Coureur;
import Modele.Semi_Marathon;
import Vues.Formulaire;

public class Evenement implements ActionListener {

	Formulaire formulaire;
	Semi_Marathon smParis = new Semi_Marathon();

	public Evenement(Formulaire formulaire) {
		this.formulaire=formulaire;
	}

	public void actionPerformed(ActionEvent e) {

		JButton bouton = (JButton) (e.getSource());
		
		/*
		 *  Listener du bouton Quitter
		 */
		if(bouton.getText().equals("Quitter")){
			System.out.println("Vous �tes sorti de l'application en appuyant sur le bouton " + bouton.getText());
			System.exit(0);
		}

		/*
		 * Listener du bouton s'inscrire
		 */
		if(bouton.getText().equals("S'inscrire"))
		{
			// En appuyant sur le bouton s'inscrire on cr�e un objet coureur
			Coureur coureur = new Coureur();
			
			/*
			 * Dans une premiere phase, on enregistre toutes les donn�es saisis dans le formulaire et on les attribut a l'objet cree
			 */
			
			if(formulaire.getCiviliteList().getSelectedItem().equals("Monsieur"))
				coureur.setSexe('H');
			if(formulaire.getCiviliteList().getSelectedItem().equals("Madame"))
				coureur.setSexe('F');
			
			coureur.setNom(formulaire.getTxtNom());
			coureur.setPrenom(formulaire.getTxtPrenom());
			coureur.setAnneeNaissance(Integer.parseInt(formulaire.getTxtAnneeNaiss()));
			coureur.setAdresse(formulaire.getTxtAdresse());
			coureur.setVille(formulaire.getTxtVille());
			coureur.setPays(formulaire.getTxtPays());
			coureur.setNationalite(formulaire.getTxtNationalite());
			coureur.setEmail(formulaire.getTxtEmail());
			coureur.setTel(formulaire.getTxtTel());
			
			if(formulaire.getChkCertifMedical().isSelected())
				coureur.setCertificatMedical(true);
			else coureur.setCertificatMedical(false);
			
			coureur.setDateInscription(new Date(e.getWhen()));
			
			/*
			 * Puis on ajoute le coureur au semi-marathon
			 */
			smParis.ajouterCoureur(coureur);
			
			/*
			 * On affiche les informations du coureur inscrit
			 */
			System.out.println("Vous venez d'inscrire le coureur suivant: ");
			System.out.println(coureur);
			
			/*
			 * Enfin, on sort la liste des coureurs inscrits
			 */
			System.out.print("\nVoici la liste des inscrits: \n");
			for (int i = 0;i<smParis.getNBMAXPARTICIPANTS();i++) {
				if (smParis.getSesInscrits()[i]!=null)
					System.out.println(smParis.getSesInscrits()[i].getNom());
			}
			
			// La methode reinitialiser est appel� en dernier lieu pour reinitialiser tous les champs
			formulaire.reinitialiser();
						
		}
	}

}
